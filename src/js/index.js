import Vue from 'vue';
import store from './store';
import router from './router';
import App from './views/App';

if (document.querySelectorAll('#app').length > 0) {
    new Vue({
        el: '#app',
        components: {
            App,
        },
        store,
        router,
    });
}