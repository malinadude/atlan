import Vue from 'vue';
import Router from 'vue-router';
import Document from './document';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        ...Document,
    ],
});