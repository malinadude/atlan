import Document from '../views/Document/Document';

const routes = [
    {
        name: 'DocumentView',
        path: '/',
        component: Document,
    },
]

export default routes