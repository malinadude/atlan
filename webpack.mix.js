const mix = require('laravel-mix');

mix.options({
        processCssUrls: false,
        autoprefixer: {
            enabled: true,
            options: {
                browsers: ['last 2 versions', '> 1%'],
                cascade: true,
                grid: true,
            }
        }
    })
    .setPublicPath('public')
    .js('src/js/index.js', 'public/js')
    .extract(['vue'])
    .sass('src/style/styles.scss', 'public/css')
    .version()
    .browserSync({
        proxy: false,
        server: {
            baseDir: './'
        },
        single: true
    });